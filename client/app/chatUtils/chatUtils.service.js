'use strict';
/* jshint -W100 */
angular.module('chatProjectApp')
  .factory('chatUtils', function ($q, $http) {
    var chatUtils = {
      /**
       * List of object types with the following properties
       * name: the name of the object type
       * deferred: true/false if the process to convert the object returns a promise.
       * matcher: Regular expression to identify object type
       * process: method that returns the string into the converted object
       */
      typeList : [
        {
          name: 'mentions',
          matcher: (/^(@)/),
          deferred: false,
          process: function(word) {
            return word.substr(1);
          }
        },
        {
          name: 'emoticons',
          matcher: (/(?=.{1,15}$)\(([^)]+)\)/),
          deferred: false,
          process: function(word) {
            return word.substr(1, word.length - 2);
          }
        },
        {
          name: 'links',
          matcher: (function() {
            return new RegExp('^(ht|f)tp(s?)\:\/\/[0-9a-zA-Z]([-.\w]*[0-9a-zA-Z])*(:(0-9)*)*(\/?)([a-zA-Z0-9\-‌​\.\?\,\'\/\\\+&amp;%\$#_]*)?$');
          })(),
          deferred: true,
          process: function(url) {
            var deferred = $q.defer();
            $http.get('/api/url', { params: { linkUrl: url } } )
              .success(function(data) {
                return deferred.resolve({url: url, title: data});
              })
              .error(function(){
                // fail silently for now
                return deferred.resolve({url: url, title: '' });
              });
            return deferred.promise;
          }
        }
      ],

      /**
       * Utility to parse a message and return an array of words
       * @param {String} msg - The message to parse
       * @returns [{String}] - An array of words
       */
      parseString : function(msg) {
        var token = (msg.match(/\S+/g));
        return token;
      },

      /**
       * Utility to process and convert a list of string to objects based on the <code>typeList</code> object types.
       * @param [String] tokens - An array of strings to process into objects
       * @returns {Object} - The converted object.
       */
      convertToObject : function(tokens) {
        var chatObject = {},
          self = this,
          deferredItems = [],
          objectDeferred = $q.defer(),
          count;

        _.forEach(tokens, function(token) {
          _.forEach(self.typeList, function(type) {
            if (type.matcher.exec(token)) {
              if (!type.deferred) {
                if (!chatObject[type.name]) {
                  chatObject[type.name] = [];
                }
                chatObject[type.name].push(type.process(token));
              } else {
                deferredItems.push({ type: type, value: token});
              }
            }
          });
        });

        if (deferredItems.length > 0) {
          count = 1;
          _.forEach(deferredItems, function(item) {
            var deferred = $q.defer();
            deferred.resolve(item.type.process(item.value));

            return deferred.promise
              .then(function(newObj) {
                if (!chatObject[item.type.name]) {
                  chatObject[item.type.name] = [];
                }
                chatObject[item.type.name].push(newObj);
                if (count === deferredItems.length) {
                  return objectDeferred.resolve(chatObject);
                } else {
                  count ++;
                }
              });
          });
        } else {
          objectDeferred.resolve(chatObject);
        }
        return objectDeferred.promise;
      }
    };
    return chatUtils;
  });
