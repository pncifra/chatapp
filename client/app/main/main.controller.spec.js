'use strict';

describe('Controller: MainCtrl', function () {

  // load the controller's module
  beforeEach(module('chatProjectApp'));

  var MainCtrl,
      scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function (_$httpBackend_, $controller, $rootScope, chatUtils) {
    scope = $rootScope.$new();
    MainCtrl = $controller('MainCtrl', {
      $scope: scope,
      chatUtils: chatUtils
    });
  }));
});
