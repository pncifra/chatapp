'use strict';

describe('Service: chatUtils', function () {

  // load the service's module
  beforeEach(module('chatProjectApp'));

  // instantiate service
  var chatUtils;
  beforeEach(inject(function (_chatUtils_) {
    chatUtils = _chatUtils_;
  }));

  it('should do something', function () {
    expect(!!chatUtils).toBe(true);
  });

});
