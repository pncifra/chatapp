
'use strict';

var _ = require('lodash');
var http = require('request');


exports.index = function(req, res) {

  /**
   * Utility to scrape the title of the site for a given url
   * @param {String} url - The url to scrape.
   * @param {Function} callback - The callback to invoke
   */
  var getLink = function (url, callback) {
    http.get({ url: url, followRedirect: true })
      .on('response', function(response) {
        var data = '';
        response.on('data', function(chunk) {
          data += chunk;
        })
        response.on('end', function() {
          callback(data);
        })
      });
  }
  // Get the title for the url
  getLink(req.param('linkUrl'), function(data) {
    var reg, response;
    if(data) {
      reg = new RegExp('<title>(.*?)</title>', 'i');
      response = data ? data.match(reg)[1] : '';
      res.send(response);
    } else {
      res.send('');
    }
  });

};