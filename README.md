# README #

### What is this repository for? ###

Message utility to convert messages to message objects

### Set up ###

Have the following installed

* Grunt : npm install grunt-cli
* bower : npm install bower
* gem : gem install sass


### Running the App ###

After pulling down the app, run the following commands in the app directory

* npm install
* bower install
* grunt serve

In a browser go to: http://localhost:9000/


![Screen Shot 2015-05-20 at 11.19.14 AM.png](https://bitbucket.org/repo/jrn74n/images/28539039-Screen%20Shot%202015-05-20%20at%2011.19.14%20AM.png)