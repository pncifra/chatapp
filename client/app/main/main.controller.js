'use strict';

angular.module('chatProjectApp')
  .controller('MainCtrl', function ($scope, $http, chatUtils) {

    $scope.message = '';
    $scope.convertedObject = {};
    $scope.messageSubmitted = false;

    /**
     * Method to submit request for message to be converted to object.
     */
    $scope.submitMessage = function() {
      $scope.messageSubmitted = true;
      var tokens = chatUtils.parseString($scope.message);
      chatUtils.convertToObject(tokens)
        .then(function(object) {
          $scope.messageSubmitted = false;
          $scope.convertedObject = object;
        });
    };

    /**
     * Method to clear the message property.
     */
    $scope.clearMessage = function(){
      $scope.message = '';
    };

  });
